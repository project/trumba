"use strict";
/**
 * @file
 * Javascript for creating Trumba Calendar Spuds.
 */
(() => {
    // Create a Trumba Spud.
    Drupal.behaviors.TrumbaAddSpud = {
        attach: function (context, settings) {
            // Find each Trumba spud and init once with the settings from the
            // Block configuration.
            once('trumba-init', '.trumba-spud', context).forEach((element) => {
                // Get the spud identifier (id).
                let spudId = element.dataset.trumbaSpud;
                // Get the settings for this particular spud id.
                let spud = settings.trumba[spudId];
                // Add the Trumba calendar for the given spud settings.
                $Trumba.addSpud(spud);
            });
        }
    };
})();
